//
//  AB4Network.swift
//  AB4SystemsDemoApp
//
//  Created by Marinescu Tudor-Andrei on 26/02/2018.
//  Copyright © 2018 Marinescu Tudor-Andrei. All rights reserved.
//

import Foundation
import Moya

let apiBaseURL = "https://api.stackexchange.com/"
let apiVersion = "2.2/"
let apiUsersRoute = "users"

enum AB4Network {
    
    case getUsers(page: Int, pageSize: Int, order: String, sort: String, site: String)
    
}

extension AB4Network: TargetType {
    
    // API URLs
    
    var baseURL: URL {return URL(string: apiBaseURL + apiVersion)!}
    
    var path: String {
        switch self {
        case .getUsers(_, _, _, _, _):
            return apiUsersRoute
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getUsers:
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getUsers:
            return Data()
        }
    }
    
    var task: Task {
        switch self {
        case .getUsers(page: let pageParam, pageSize: let pageSizeParam, order: let orderParam, sort: let sortParam, site: let siteParam):
            return .requestParameters(parameters: ["page" : pageParam, "pageSize" : pageSizeParam, "order" : orderParam, "sort" : sortParam, "site" : siteParam], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String: String]? {
        switch self {
        case .getUsers(_, _, _, _, _):
            return nil
        }
    }
    
}
