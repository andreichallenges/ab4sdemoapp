//
//  APIManager.swift
//  AB4SystemsDemoApp
//
//  Created by Marinescu Tudor-Andrei on 26/02/2018.
//  Copyright © 2018 Marinescu Tudor-Andrei. All rights reserved.
//

import Foundation
import Moya
import Moya_ObjectMapper

class APIManager {
    
    static let provider = MoyaProvider<AB4Network>(endpointClosure: { (target: AB4Network) -> Endpoint<AB4Network> in
        return MoyaProvider.defaultEndpointMapping(for: target)
    })
    
    var usersCache = AB4Cache.shared
    
    func getUsers(page: Int, pageSize: Int, order: String, sort: String, site: String, completion: @escaping (_ success: Bool, _ users: [SOUserModel?]?, _ error: MoyaError?) -> Void) {
        var users: [SOUserModel?] = []
        if let cachedUsersData = usersCache["AB4Users"] {
            do {
                let usersArray = try JSONSerialization.jsonObject(with: cachedUsersData, options: .allowFragments) as! [[String: Any]]
                usersArray.forEach {
                    users.append(SOUserModel(JSON: $0))
                }
                completion(true, users, nil)
                return
                
            } catch {
                completion(false, nil, MoyaError.requestMapping("Failed to fetch from cache"))
            }
        } else {
            APIManager.provider.request(.getUsers(page: page, pageSize: pageSize, order: order, sort: sort, site: site), callbackQueue: DispatchQueue.global(qos: .background), progress: nil) { (result) in
                switch result {
                case let .success(response):
                    do {
                        
                        _ = try response.filterSuccessfulStatusAndRedirectCodes()
                        let data = try response.mapJSON() as? [String : Any]
                        guard let items = data!["items"] as? [[String : Any]] else {
                            throw MoyaError.jsonMapping(response)
                        }
                        self.usersCache["AB4Users"] = try JSONSerialization.data(withJSONObject: items, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        items.forEach {
                            users.append(SOUserModel(JSON: $0))
                            
                        }
                        DispatchQueue.main.async {
                            completion(true, users, nil)
                        }
                    } catch {
                        DispatchQueue.main.async {
                            completion(false, nil, MoyaError.statusCode(response))
                        }
                    }
                case let .failure(error):
                    DispatchQueue.main.async {
                        completion(false, nil, error)
                    }
                }
            }
        }
    }
    
}

