//
//  MasterViewController.swift
//  AB4SystemsDemoApp
//
//  Created by Marinescu Tudor-Andrei on 26/02/2018.
//  Copyright © 2018 Marinescu Tudor-Andrei. All rights reserved.
//

import UIKit
import SVProgressHUD

class MasterViewController: UIViewController {

    fileprivate var collapseDetailViewController = true

    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    var detailViewController: DetailViewController? = nil
    var objects = [SOUserModel]()
    var apiManager = APIManager()

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Users"
        splitViewController?.delegate = self
        
        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork(){
            apiManager.getUsers(page: 1, pageSize: 10, order: "desc", sort: "reputation", site: "stackoverflow") {[weak self] (success, users, error) in
                switch success {
                case true:
                    SVProgressHUD.showSuccess(withStatus: "Users fetched!")
                    if users != nil {
                        self?.objects = users as! [SOUserModel]
                        self?.tableView.reloadData()
                    }
                case false:
                    SVProgressHUD.showError(withStatus: "Users couldn't be fetched!")
                }
            }
        } else {
            SVProgressHUD.showError(withStatus: "Internet Connection not Available!")
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        guard let navController = segue.destination as? UINavigationController,
            let viewController = navController.topViewController as? DetailViewController
            else {
                fatalError("Expected DetailViewController")
        }
        collapseDetailViewController = false
        viewController.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        viewController.navigationItem.leftItemsSupplementBackButton = true
        if let indexPath = tableView.indexPathForSelectedRow {
        let user = objects[indexPath.row]
        viewController.userItem = user
        viewController.title = user.name
        }
    }
    
}

extension MasterViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UserTableViewCell
        cell.configureCell(user: objects[indexPath.row])
        return cell
    }
}

extension MasterViewController: UITableViewDelegate {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
    }
}

extension MasterViewController: UISplitViewControllerDelegate {
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return collapseDetailViewController
    }
}
