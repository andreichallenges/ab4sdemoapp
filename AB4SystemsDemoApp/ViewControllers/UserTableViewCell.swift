//
//  UserTableViewCell.swift
//  AB4SystemsDemoApp
//
//  Created by Marinescu Tudor-Andrei on 26/02/2018.
//  Copyright © 2018 Marinescu Tudor-Andrei. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func configureCell(user: SOUserModel) {
        userNameLabel?.text = user.name
        userImageView?.af_setImage(
            withURL: URL(string: user.profilePictureLink!)!,
            placeholderImage: UIImage(named: "UserPlaceholderImage"),
            filter: nil,
            imageTransition: .noTransition
        )
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        userNameLabel?.text = nil
        userImageView?.af_cancelImageRequest()
        userImageView?.layer.removeAllAnimations()
        userImageView?.image = nil
    }

}
