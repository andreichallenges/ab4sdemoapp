//
//  DetailViewController.swift
//  AB4SystemsDemoApp
//
//  Created by Marinescu Tudor-Andrei on 26/02/2018.
//  Copyright © 2018 Marinescu Tudor-Andrei. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var goldLabel: UILabel!
    @IBOutlet weak var silverLabel: UILabel!
    @IBOutlet weak var bronzeLabel: UILabel!
    
    
    func configureView() {
        if let user = userItem {
            if let gLabel = goldLabel {
                gLabel.text = "Gold: \(user.goldBadges ?? 0)"
            }
            if let sLabel = silverLabel {
                sLabel.text = "Silver: \(user.silverBadges ?? 0)"
            }
            if let bLabel = bronzeLabel {
                bLabel.text = "Bronze: \(user.bronzeBadges ?? 0)"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var userItem: SOUserModel? {
        didSet {
            configureView()
        }
    }


}
