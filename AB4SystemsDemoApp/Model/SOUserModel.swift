//
//  SOUserModel.swift
//  AB4SystemsDemoApp
//
//  Created by Marinescu Tudor-Andrei on 26/02/2018.
//  Copyright © 2018 Marinescu Tudor-Andrei. All rights reserved.
//


import Foundation
import ObjectMapper

struct SOUserModel: Mappable {
    
    var bronzeBadges : Int?
    var silverBadges: Int?
    var goldBadges : Int?
    var accountID : Int?
    var profilePictureLink : String?
    var name: String?
    var location: String?
    
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        
        bronzeBadges <- map["badge_counts.bronze"]
        silverBadges <- map["badge_counts.silver"]
        goldBadges <- map["badge_counts.gold"]
        accountID <- map["account_id"]
        profilePictureLink <- map["profile_image"]
        name <- map["display_name"]
        location <- map["location"]
    }
    
}
